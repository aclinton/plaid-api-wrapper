<?php


namespace PlaidApiWrapper;


use BVAccel\JsonApiWrapper\Contracts\ClientInterface;
use Psr\Http\Message\ResponseInterface;

class Client implements ClientInterface
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected $client;

    /**
     * @var string
     */
    private $client_id;

    /**
     * @var string
     */
    private $secret;

    /**
     * Client constructor.
     *
     * @param ClientConfig $config
     */
    public function __construct(ClientConfig $config)
    {
        $this->client_id = $config->getClientId();
        $this->secret    = $config->getClientSecret();
        $this->client    = new \GuzzleHttp\Client([
            'base_uri'   => $config->getBaseUri(),
            'exceptions' => false
        ]);
    }

    /**
     * Get
     *
     * Perform GET request and return response
     *
     * @param string $uri
     * @param array $query_params
     * @return ResponseInterface
     */
    public function get(string $uri, array $query_params = []): ResponseInterface
    {
        throw new \Exception("Not implemented");
    }

    /**
     * Post
     *
     * Perform POST request and return response
     *
     * @param string $uri
     * @param array $json_payload
     * @param bool $authorized
     * @return ResponseInterface
     */
    public function post(string $uri, array $json_payload, bool $authorized = true): ResponseInterface
    {
        if ($authorized) {
            $json_payload['client_id'] = $this->client_id;
            $json_payload['secret']    = $this->secret;
        }

        return $this->client->post($uri, [
            'json' => empty($json_payload) ? new \stdClass() : $json_payload
        ]);
    }

    /**
     * Post
     *
     * Perform PUT request and return response
     *
     * @param string $uri
     * @param array $json_payload
     * @return ResponseInterface
     */
    public function put(string $uri, array $json_payload): ResponseInterface
    {
        throw new \Exception("Not implemented");
    }

    /**
     * Delete
     *
     * Perform DELETE request and return response
     *
     * @param string $uri
     * @return ResponseInterface
     */
    public function delete(string $uri): ResponseInterface
    {
        throw new \Exception("Not implemented");
    }
}