<?php


namespace PlaidApiWrapper;


class ClientConfig
{
    /**
     * @var
     */
    private $base_uri;

    /**
     * @var
     */
    private $client_id;

    /**
     * @var
     */
    private $client_secret;

    /**
     * ClientConfig constructor.
     *
     * @param $base_uri
     * @param $client_id
     * @param $client_secret
     */
    public function __construct($base_uri, $client_id, $client_secret)
    {
        $this->client_secret = $client_secret;
        $this->base_uri      = $base_uri;
        $this->client_id     = $client_id;
    }

    /**
     * @return mixed
     */
    public function getBaseUri()
    {
        return $this->base_uri;
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->client_id;
    }

    /**
     * @return mixed
     */
    public function getClientSecret()
    {
        return $this->client_secret;
    }
}