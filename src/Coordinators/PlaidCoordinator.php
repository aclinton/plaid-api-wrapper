<?php


namespace PlaidApiWrapper\Coordinators;


use PlaidApiWrapper\ClientConfig;
use PlaidApiWrapper\Client;
use PlaidApiWrapper\Parameters\InstitutionParameters;
use PlaidApiWrapper\Parameters\TransactionParameters;
use PlaidApiWrapper\Requests\AccountRequest;
use PlaidApiWrapper\Requests\CategoryRequest;
use PlaidApiWrapper\Requests\InstitutionRequest;
use PlaidApiWrapper\Requests\ItemRequest;
use PlaidApiWrapper\Requests\PublicTokenRequest;
use PlaidApiWrapper\Requests\TransactionRequest;

class PlaidCoordinator
{
    /**
     * @var Client
     */
    private $client;

    /**
     * PlaidCoordinator constructor.
     *
     * @param ClientConfig $config
     */
    public function __construct(ClientConfig $config)
    {
        $this->client = new Client($config);
    }

    /**
     * @return \PlaidApiWrapper\Requests\PublicTokenRequest
     */
    public function publicToken(): PublicTokenRequest
    {
        return new PublicTokenRequest(
            $this->client
        );
    }

    /**
     * @return \PlaidApiWrapper\Requests\AccountRequest
     */
    public function account(): AccountRequest
    {
        return new AccountRequest($this->client);
    }

    /**
     * @return \PlaidApiWrapper\Requests\TransactionRequest
     */
    public function transaction(): TransactionRequest
    {
        return new TransactionRequest($this->client);
    }

    /**
     * @return \PlaidApiWrapper\Parameters\TransactionParameters
     */
    public function transactionParameters(): TransactionParameters
    {
        return new TransactionParameters();
    }

    /**
     * @return \PlaidApiWrapper\Requests\ItemRequest
     */
    public function item(): ItemRequest
    {
        return new ItemRequest($this->client);
    }

    /**
     * @return \PlaidApiWrapper\Requests\InstitutionRequest
     */
    public function institution(): InstitutionRequest
    {
        return new InstitutionRequest($this->client);
    }

    /**
     * @return \PlaidApiWrapper\Parameters\InstitutionParameters
     */
    public function institutionParameters(): InstitutionParameters
    {
        return new InstitutionParameters();
    }

    /**
     * @return \PlaidApiWrapper\Requests\CategoryRequest
     */
    public function category()
    {
        return new CategoryRequest($this->client);
    }
}