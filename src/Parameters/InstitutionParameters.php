<?php


namespace PlaidApiWrapper\Parameters;


use PlaidApiWrapper\Parameters\Traits\CountParameter;
use PlaidApiWrapper\Parameters\Traits\OffsetParameter;

class InstitutionParameters extends ParameterBuilder
{
    use CountParameter, OffsetParameter;

    /**
     * @param bool $include
     * @return $this
     */
    public function includeOptionalMetadata(bool $include): self
    {
        $this->setParameter('options.include_optional_metadata', $include);

        return $this;
    }
}