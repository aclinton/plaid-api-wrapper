<?php


namespace PlaidApiWrapper\Parameters;


use Illuminate\Support\Arr;

/**
 * Class ParameterBuilder
 *
 * @package BVAccel\ShopifyApiWrapper\Parameters
 */
abstract class ParameterBuilder
{
    /**
     * @var array
     */
    protected $parameters = [];

    /**
     * Set Parameter
     *
     * @param $key
     * @param $value
     */
    public function setParameter($key, $value): void
    {
        $this->parameters[ $key ] = $value;
    }

    /**
     * Get Parameter
     *
     * @return array
     */
    public function getParameters(): array
    {
        $data = [];

        foreach($this->parameters as $key => $v) {
            $data = Arr::add($data ,$key, $v);
        }

        return $data;
    }
}
