<?php


namespace PlaidApiWrapper\Parameters\Traits;


trait CountParameter
{
    public function count($count)
    {
        $this->setParameter('count', $count);

        return $this;
    }
}