<?php


namespace PlaidApiWrapper\Parameters\Traits;


trait OffsetParameter
{
    public function offset($offset)
    {
        $this->setParameter('offset', $offset);

        return $this;
    }
}