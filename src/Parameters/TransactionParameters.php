<?php


namespace PlaidApiWrapper\Parameters;


class TransactionParameters extends ParameterBuilder
{
    /**
     * @param string $date
     * @return $this
     */
    public function startDate(string $date): self
    {
        $this->setParameter('start_date', $date);

        return $this;
    }

    /**
     * @param string $date
     * @return $this
     */
    public function endDate(string $date): self
    {
        $this->setParameter('end_date', $date);

        return $this;
    }

    /**
     * @param $ids
     * @return $this
     */
    public function accountIds($ids): self
    {
        $this->setParameter('options.account_ids', func_get_args());

        return $this;
    }

    /**
     * @param int $count
     * @return $this
     */
    public function count(int $count): self
    {
        $this->setParameter('options.count', $count);

        return $this;
    }

    /**
     * @param int $offset
     * @return $this
     */
    public function offset(int $offset): self
    {
        $this->setParameter('options.offset', $offset);

        return $this;
    }
}