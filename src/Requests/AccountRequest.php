<?php


namespace PlaidApiWrapper\Requests;


use PlaidApiWrapper\Responses\AccountsResponse;

class AccountRequest extends BaseRequest
{
    /**
     * @param string $access_token
     * @return AccountsResponse
     */
    public function all(string $access_token): AccountsResponse
    {
        return new AccountsResponse($this->client->post('/accounts/get', [
            'access_token' => $access_token
        ]));
    }
}