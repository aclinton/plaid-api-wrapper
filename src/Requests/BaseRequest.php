<?php


namespace PlaidApiWrapper\Requests;


use PlaidApiWrapper\Client;

class BaseRequest
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * BaseRequest constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }
}