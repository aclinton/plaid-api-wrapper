<?php


namespace PlaidApiWrapper\Requests;


use PlaidApiWrapper\Responses\MultipleCategoryResponse;

class CategoryRequest extends BaseRequest
{
    /**
     * @return MultipleCategoryResponse
     */
    public function all()
    {
        return new MultipleCategoryResponse(
            $this->client->post('categories/get', [], false)
        );
    }
}