<?php


namespace PlaidApiWrapper\Requests;


use PlaidApiWrapper\Parameters\InstitutionParameters;
use PlaidApiWrapper\Responses\MultipleInstitutionResponse;
use PlaidApiWrapper\Responses\SingleInstitutionResponse;

class InstitutionRequest extends BaseRequest
{
    /**
     * @param string $public_key
     * @param string $institution_id
     * @return SingleInstitutionResponse
     */
    public function find(string $public_key, string $institution_id)
    {
        return new SingleInstitutionResponse(
            $this->client->post('institutions/get_by_id', [
                'public_key'     => $public_key,
                'institution_id' => $institution_id
            ])
        );
    }

    /**
     * @param InstitutionParameters|null $params
     * @return MultipleInstitutionResponse
     */
    public function all(?InstitutionParameters $params = null)
    {
        return new MultipleInstitutionResponse(
            $this->client->post('institutions/get', $params ? $params->getParameters() : [])
        );
    }
}