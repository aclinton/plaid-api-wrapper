<?php


namespace PlaidApiWrapper\Requests;


use PlaidApiWrapper\Responses\SingleItemResponse;

class ItemRequest extends BaseRequest
{
    /**
     * @param string|null $access_token
     * @return SingleItemResponse
     * @throws \Exception
     */
    public function find(?string $access_token = null): SingleItemResponse
    {
        return new SingleItemResponse(
            $this->client->post('item/get', [
                'access_token' => $access_token
            ])
        );
    }
}