<?php

namespace PlaidApiWrapper\Requests;

use PlaidApiWrapper\Responses\ExchangeTokenResponse;

class PublicTokenRequest extends BaseRequest
{
    /**
     * @param string $public_token
     * @return ExchangeTokenResponse
     */
    public function exchange(string $public_token): ExchangeTokenResponse
    {
        return new ExchangeTokenResponse($this->client->post('/item/public_token/exchange', [
            'public_token' => $public_token,
        ]));
    }
}
