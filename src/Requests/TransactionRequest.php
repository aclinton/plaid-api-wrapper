<?php


namespace PlaidApiWrapper\Requests;


use PlaidApiWrapper\Parameters\TransactionParameters;
use PlaidApiWrapper\Responses\TransactionsResponse;

class TransactionRequest extends BaseRequest
{
    /**
     * @param string $access_token
     * @param TransactionParameters $parameters
     * @return TransactionsResponse
     */
    public function all(string $access_token, ?TransactionParameters $parameters = null): TransactionsResponse
    {
        return new TransactionsResponse(
            $this->client->post('/transactions/get',
            array_merge(
                ['access_token' => $access_token],
                $parameters ? $parameters->getParameters() : []
            )
        ));
    }
}