<?php


namespace PlaidApiWrapper\Resources;


use BVAccel\JsonApiWrapper\Resources\JsonResource;

/**
 * Class Account
 *
 * @package PlaidApiWrapper\Resources
 * @property string $account_id
 * @property Balance $balances
 * @property string $mask
 * @property string $name
 * @property string $official_name
 * @property string $subtype
 * @property string $type
 */
class Account extends JsonResource
{
    /**
     * Define Resource Properties
     *
     * @return array
     */
    protected function getPropertyDefinitions(): array
    {
        return [
            'account_id'    => 'basic|string',
            'balances'      => 'resource|' . Balance::class,
            'mask'          => 'basic|string',
            'name'          => 'basic|string',
            'official_name' => 'basic|string',
            'subtype'       => 'basic|string',
            'type'          => 'basic|string',
        ];
    }
}