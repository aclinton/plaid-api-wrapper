<?php


namespace PlaidApiWrapper\Resources;


use BVAccel\JsonApiWrapper\Resources\JsonResource;

/**
 * Class Balance
 *
 * @package PlaidApiWrapper\Resources
 * @property integer available
 * @property integer current
 * @property integer limit
 * @property string iso_currency_code
 * @property string unofficial_currency_code
 */
class Balance extends JsonResource
{
    /**
     * Define Resource Properties
     *
     * @return array
     */
    protected function getPropertyDefinitions(): array
    {
        return [
            'available'                => 'basic|integer',
            'current'                  => 'basic|integer',
            'limit'                    => 'basic|integer',
            'iso_currency_code'        => 'basic|string',
            'unofficial_currency_code' => 'basic|string'
        ];
    }
}