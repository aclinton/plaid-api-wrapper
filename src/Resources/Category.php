<?php


namespace PlaidApiWrapper\Resources;


use BVAccel\JsonApiWrapper\Resources\JsonResource;

/**
 * Class Category
 *
 * @package PlaidApiWrapper\Resources
 * @property string $group
 * @property array $hierarchy
 * @property string $category_id
 */
class Category extends JsonResource
{
    /**
     * Define Resource Properties
     *
     * @return array
     */
    protected function getPropertyDefinitions(): array
    {
        return [
            'group'       => 'basic|string',
            'hierarchy'   => 'basic-array|string',
            'category_id' => 'basic|string,integer'
        ];
    }
}