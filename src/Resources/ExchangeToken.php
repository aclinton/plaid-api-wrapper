<?php


namespace PlaidApiWrapper\Resources;


use BVAccel\JsonApiWrapper\Resources\JsonResource;

/**
 * Class ExchangeToken
 *
 * @package PlaidApiWrapper\Resources
 * @property string $access_token
 * @property string $item_id
 * @property string $request_id
 */
class ExchangeToken extends JsonResource
{
    /**
     * Define Resource Properties
     *
     * @return array
     */
    protected function getPropertyDefinitions(): array
    {
        return [
            'access_token' => 'basic|string|readonly',
            'item_id'      => 'basic|string|readonly',
            'request_id'   => 'basic|string|readonly'
        ];
    }
}