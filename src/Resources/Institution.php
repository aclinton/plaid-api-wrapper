<?php


namespace PlaidApiWrapper\Resources;


use BVAccel\JsonApiWrapper\Resources\JsonResource;

/**
 * Class Institution
 *
 * @package PlaidApiWrapper\Resources
 * @property boolean $has_mfa
 * @property array $mfa
 * @property string $mfa_code_type
 * @property string $institution_id
 * @property string $name
 * @property array $products
 * @property string $primary_color
 * @property string $url
 * @property string $logo
 * @property InstitutionCredential[] $credentials
 */
class Institution extends JsonResource
{
    /**
     * Define Resource Properties
     *
     * @return array
     */
    protected function getPropertyDefinitions(): array
    {
        return [
            'credentials'    => 'resource-array|' . InstitutionCredential::class,
            'has_mfa'        => 'basic|boolean',
            'mfa'            => 'basic-array|string',
            'mfa_code_type'  => 'basic|string',
            'institution_id' => 'basic|string',
            'name'           => 'basic|string',
            'products'       => 'basic-array|string',
            'primary_color'  => 'basic|string',
            'url'            => 'basic|string',
            'logo'           => 'basic|string',
        ];
    }
}