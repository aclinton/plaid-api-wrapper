<?php


namespace PlaidApiWrapper\Resources;


use BVAccel\JsonApiWrapper\Resources\JsonResource;

/**
 * Class InstitutionCredential
 *
 * @package PlaidApiWrapper\Resources
 * @property string $label
 * @property string $name
 * @property string $type
 */
class InstitutionCredential extends JsonResource
{
    /**
     * Define Resource Properties
     *
     * @return array
     */
    protected function getPropertyDefinitions(): array
    {
        return [
            'label' => 'basic|string',
            'name'  => 'basic|string',
            'type'  => 'basic|string',
        ];
    }
}