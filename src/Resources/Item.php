<?php


namespace PlaidApiWrapper\Resources;


use BVAccel\JsonApiWrapper\Resources\JsonResource;

/**
 * Class Item
 *
 * @package PlaidApiWrapper\Resources
 * @property array $available_products
 * @property array $billed_products
 * @property string $error
 * @property string $institution_id
 * @property string $item_id
 * @property string $webhook
 */
class Item extends JsonResource
{
    /**
     * Define Resource Properties
     *
     * @return array
     */
    protected function getPropertyDefinitions(): array
    {
        return [
            'available_products' => 'basic-array|string',
            'billed_products'    => 'basic-array|string',
            'error'              => 'basic|string,boolean',
            'institution_id'     => 'basic|string,integer',
            'item_id'            => 'basic|string',
            'webhook'            => 'basic|string',
        ];
    }
}