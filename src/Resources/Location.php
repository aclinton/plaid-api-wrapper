<?php


namespace PlaidApiWrapper\Resources;


use BVAccel\JsonApiWrapper\Resources\JsonResource;

/**
 * Class Location
 *
 * @package PlaidApiWrapper\Resources
 * @property string address
 * @property string city
 * @property string state
 * @property string zip
 * @property string lat
 * @property string lon
 */
class Location extends JsonResource
{
    /**
     * Define Resource Properties
     *
     * @return array
     */
    protected function getPropertyDefinitions(): array
    {
        return [
            'address' => 'basic|string',
            'city'    => 'basic|string',
            'state'   => 'basic|string',
            'zip'     => 'basic|string',
            'lat'     => 'basic|double',
            'lon'     => 'basic|double',
        ];
    }
}