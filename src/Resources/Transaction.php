<?php


namespace PlaidApiWrapper\Resources;


use BVAccel\JsonApiWrapper\Resources\JsonResource;

/**
 * Class Transaction
 *
 * @package PlaidApiWrapper\Resources
 * @property string account_id
 * @property double amount
 * @property string iso_currency_code
 * @property string unofficial_currency_code
 * @property array category
 * @property integer category_id
 * @property string date
 * @property array location
 * @property string name
 * @property array payment_meta
 * @property boolean pending
 * @property string pending_transaction_id
 * @property string account_owner
 * @property string transaction_id
 * @property string transaction_type
 */
class Transaction extends JsonResource
{
    /**
     * Define Resource Properties
     *
     * @return array
     */
    protected function getPropertyDefinitions(): array
    {
        return [
            'account_id'               => 'basic|string',
            'amount'                   => 'basic|double',
            'iso_currency_code'        => 'basic|string',
            'unofficial_currency_code' => 'basic|string',
            'category'                 => 'basic-array|string',
            'category_id'              => 'basic|integer',
            'date'                     => 'basic|string',
            'location'                 => 'resource|' . Location::class,
            'name'                     => 'basic|string',
            'payment_meta'             => 'basic-array|string',
            'pending'                  => 'basic|boolean',
            'pending_transaction_id'   => 'basic|string',
            'account_owner'            => 'basic|string',
            'transaction_id'           => 'basic|string',
            'transaction_type'         => 'basic|string',
        ];
    }
}