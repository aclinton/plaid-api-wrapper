<?php


namespace PlaidApiWrapper\Resources;


use BVAccel\JsonApiWrapper\Resources\JsonResource;

/**
 * Class TransactionsMeta
 *
 * @package PlaidApiWrapper\Resources
 * @property string $request_id
 * @property int $total_transactions
 * @property Transaction[] $transactions
 * @property Account[] $accounts
 */
class TransactionsMeta extends JsonResource
{
    /**
     * Define Resource Properties
     *
     * @return array
     */
    protected function getPropertyDefinitions(): array
    {
        return [
            'request_id'         => 'basic|string',
            'total_transactions' => 'basic|integer',
            'transactions'       => 'resource-array|' . Transaction::class,
            'accounts'           => 'resource-array|' . Account::class,
        ];
    }
}