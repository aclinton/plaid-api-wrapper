<?php


namespace PlaidApiWrapper\Responses;


use PlaidApiWrapper\Resources\ExchangeToken;
use BVAccel\JsonApiWrapper\Responses\SingleResourceResponse;

class ExchangeTokenResponse extends SingleResourceResponse
{
    /**
     * @return ExchangeToken
     */
    public function exchange(): ExchangeToken
    {
        return $this->data;
    }

    /**
     * Get Data
     * d
     * Define what the root data point is. This will be passed into the base resource constructor
     *
     * @param array $base_data
     * @return array
     */
    protected function getData(array $base_data): array
    {
        return $base_data;
    }

    /**
     * Get Base Resource
     *
     * Define what the base resource object is.
     *
     * @return string
     */
    protected function getBaseResource(): string
    {
        return ExchangeToken::class;
    }
}