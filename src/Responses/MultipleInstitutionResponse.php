<?php


namespace PlaidApiWrapper\Responses;


use PlaidApiWrapper\Resources\Institution;
use BVAccel\JsonApiWrapper\Responses\MultipleResourceResponse;

class MultipleInstitutionResponse extends MultipleResourceResponse
{
    /**
     * @return \BVAccel\JsonApiWrapper\Resources\JsonResource[]|Institution[]
     */
    public function institutions(): array
    {
        return $this->data;
    }

    /**
     * Get Data
     *
     * Define what the root data point is. This will be passed into the base resource constructor
     *
     * @param array $base_data
     * @return array
     */
    protected function getData(array $base_data): array
    {
        return $base_data['institutions'] ?? [];
    }

    /**
     * Get Base Resource
     *
     * Define what the base resource object is.
     *
     * @return string
     */
    protected function getBaseResource(): string
    {
        return Institution::class;
    }
}