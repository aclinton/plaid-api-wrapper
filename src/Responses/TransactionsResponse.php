<?php


namespace PlaidApiWrapper\Responses;


use PlaidApiWrapper\Resources\TransactionsMeta;
use BVAccel\JsonApiWrapper\Responses\SingleResourceResponse;

class TransactionsResponse extends SingleResourceResponse
{
    /**
     * @return \BVAccel\JsonApiWrapper\Resources\JsonResource|TransactionsMeta
     */
    public function responseData()
    {
        return $this->data;
    }

    /**
     * Get Data
     *
     * Define what the root data point is. This will be passed into the base resource constructor
     *
     * @param array $base_data
     * @return array
     */
    protected function getData(array $base_data): array
    {
        return $base_data;
    }

    /**
     * Get Base Resource
     *
     * Define what the base resource object is.
     *
     * @return string
     */
    protected function getBaseResource(): string
    {
        return TransactionsMeta::class;
    }
}